var PlayerFormModule = angular.module('PlayerFormModule', []);

PlayerFormModule.controller('PlayerFormController', Components.PlayerFormController);
PlayerFormModule.directive('playerForm', Components.PlayerFormDirective);